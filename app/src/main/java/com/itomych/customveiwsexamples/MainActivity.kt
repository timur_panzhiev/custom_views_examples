package com.itomych.customveiwsexamples

import android.content.Intent
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.Rect
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.res.ResourcesCompat
import com.itomych.customveiwsexamples.surfaceView.SurfaceViewActivity
import kotlinx.android.synthetic.main.activity_main.*
import kotlin.math.min

private const val OFFSET = 40
private const val MULTIPLIER = 100

class MainActivity : AppCompatActivity() {

    private lateinit var mCanvas: Canvas
    private val mPaint = Paint(Paint.ANTI_ALIAS_FLAG)
    private val mPaintText = Paint(Paint.UNDERLINE_TEXT_FLAG)
    private lateinit var mBitmap: Bitmap
    private val mRect = Rect()
    private val mBounds = Rect()

    private var mOffset = OFFSET

    private var mColorBackground: Int = 0
    private var mColorRectangle: Int = 0
    private var mColorAccent: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        button_animate.setOnClickListener { progress.startAnimation() }
        draw_button.setOnClickListener{
            startActivity(Intent(this, DrawingActivity::class.java))
        }

        clipping_button.setOnClickListener{
            startActivity(Intent(this, ClippedViewActivity::class.java))
        }

        surface_view_button.setOnClickListener{
            startActivity(Intent(this, SurfaceViewActivity::class.java))
        }

        video_view_button.setOnClickListener{
            startActivity(Intent(this, VideoActivity::class.java))
        }

        camera_button.setOnClickListener{
            startActivity(Intent(this, CameraXActivity::class.java))
        }

        mColorBackground = ResourcesCompat.getColor(
            resources,
            R.color.colorBackground, null
        )
        mColorRectangle = ResourcesCompat.getColor(
            resources,
            R.color.colorRectangle, null
        )
        mColorAccent = ResourcesCompat.getColor(
            resources,
            R.color.colorAccent, null
        )
        mPaint.color = mColorBackground
        mPaintText.apply {
            color = ResourcesCompat.getColor(
                resources,
                R.color.colorPrimaryDark, null
            )
            textSize = 40f
        }
    }

    fun drawSomething(view: View) {
        val vWidth = view.width
        val vHeight = view.height
        val halfWidth = vWidth / 2f
        val halfHeight = vHeight / 2f

        if (mOffset == OFFSET) {
            mBitmap = Bitmap.createBitmap(vWidth, vHeight, Bitmap.Config.ARGB_8888)
            canvas_image_view.setImageBitmap(mBitmap)
            mCanvas = Canvas(mBitmap)
            mCanvas.drawColor(mColorBackground)
            mCanvas.drawText(getString(R.string.keep_tapping), 20f, 50f, mPaintText)
            mOffset += OFFSET

        } else {
            if (mOffset < halfWidth && mOffset < halfHeight) {
                mPaint.color = mColorRectangle - MULTIPLIER * mOffset
                mRect.set(
                    mOffset, mOffset, vWidth - mOffset, vHeight - mOffset
                )
                mCanvas.drawRect(mRect, mPaint)
                mOffset += OFFSET
            } else {
                mPaint.color = mColorAccent
                mCanvas.drawCircle(halfWidth, halfHeight, min(halfWidth, halfHeight) / 3f, mPaint)
                val text = getString(R.string.done)

                mPaintText.getTextBounds(text, 0, text.length, mBounds)
                // Calculate x and y for text so it's centered.
                val x = halfWidth - mBounds.centerX()
                val y = halfHeight - mBounds.centerY()
                mCanvas.drawText(text, x, y, mPaintText)
            }
        }
        view.invalidate()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        tumbler.setSelectionCount(item.order)
        return super.onOptionsItemSelected(item)
    }
}
