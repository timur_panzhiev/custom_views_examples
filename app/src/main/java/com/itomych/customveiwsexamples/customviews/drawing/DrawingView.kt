package com.itomych.customveiwsexamples.customviews.drawing

import android.content.Context
import android.util.AttributeSet
import androidx.constraintlayout.widget.ConstraintLayout
import com.itomych.customveiwsexamples.R
import kotlinx.android.synthetic.main.drawing_canvas_view_layout.view.clean_canvas_ib as cleanButton
import kotlinx.android.synthetic.main.drawing_canvas_view_layout.view.drawing_canvas_view as drawingCanvasView
import kotlinx.android.synthetic.main.drawing_canvas_view_layout.view.save_bitmap_ib as saveButton

class DrawingView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : ConstraintLayout(context, attrs, defStyleAttr) {

    init {
        inflate(context, R.layout.drawing_canvas_view_layout, this)
        setupListeners()
    }

    private fun setupListeners() {
        cleanButton.setOnClickListener {
            drawingCanvasView.cleanCanvas()
        }
        saveButton.setOnClickListener {
            drawingCanvasView.saveBitmap()
        }
    }
}