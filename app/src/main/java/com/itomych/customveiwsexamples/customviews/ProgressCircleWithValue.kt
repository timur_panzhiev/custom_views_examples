package com.itomych.customveiwsexamples.customviews

import android.content.Context
import android.util.AttributeSet
import android.widget.FrameLayout
import android.widget.LinearLayout
import android.widget.TextView
import com.itomych.customveiwsexamples.R


class ProgressCircleWithValue @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : FrameLayout(context, attrs, defStyleAttr) {

    private val mCircle: ProgressCircle? = null
    private val mValuesLayout: LinearLayout? = null
    private val mValue: TextView? = null
    private val mMetrics: TextView? = null

    init {
        inflate(getContext(), R.layout.progress_circle_with_value, this)
    }
}