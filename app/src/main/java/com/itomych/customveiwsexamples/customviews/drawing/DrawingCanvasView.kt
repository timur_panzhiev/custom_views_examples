package com.itomych.customveiwsexamples.customviews.drawing

import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.Path
import android.net.Uri
import android.os.Environment.DIRECTORY_PICTURES
import android.os.Environment.getExternalStoragePublicDirectory
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.View
import androidx.core.content.res.ResourcesCompat
import java.io.File
import java.io.FileOutputStream
import kotlin.math.abs


private const val TOUCH_TOLERANCE = 4f

class DrawingCanvasView @JvmOverloads constructor(
    context: Context,
    attributes: AttributeSet? = null,
    def: Int = 0
) :
    View(context, attributes, def) {

    private var mPaint: Paint = Paint()
    private var mPath: Path = Path()
    private var mDrawColor: Int = 0
    private var mBackgroundColor: Int = 0
    private lateinit var mExtraCanvas: Canvas
    private lateinit var mExtraBitmap: Bitmap
    private var mX = 0f
    private var mY = 0f

    init {
        mBackgroundColor = ResourcesCompat.getColor(
            resources,
            com.itomych.customveiwsexamples.R.color.colorWhite, null
        )
        mDrawColor = ResourcesCompat.getColor(
            resources,
            com.itomych.customveiwsexamples.R.color.opaque_yellow, null
        )
        // Set up the paint with which to draw.
        mPaint.apply {
            color = mDrawColor
            // Smoothes out edges of what is drawn without affecting shape.
            isAntiAlias = true
            // Dithering affects how colors with higher-precision device
            // than the are down-sampled.
            isDither = true
            style = Paint.Style.STROKE // default: FILL
            strokeJoin = Paint.Join.ROUND // default: MITER
            strokeCap = Paint.Cap.ROUND // default: BUTT
            strokeWidth = 12f // default: Hairline-width (really thin)
        }
    }

    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        super.onSizeChanged(w, h, oldw, oldh)
        setCanvasToDefaultState()
    }

    private fun setCanvasToDefaultState() {
        mExtraBitmap = Bitmap.createBitmap(
            width, height,
            Bitmap.Config.ARGB_8888
        )

        mExtraCanvas = Canvas(mExtraBitmap)
        mExtraCanvas.drawColor(mBackgroundColor)
    }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)
        canvas.drawBitmap(mExtraBitmap, 0f, 0f, null)
    }

    override fun onTouchEvent(event: MotionEvent): Boolean {
        val x = event.x
        val y = event.y

        when (event.action) {
            MotionEvent.ACTION_DOWN -> touchStart(x, y)
            MotionEvent.ACTION_MOVE -> touchMove(x, y)
            MotionEvent.ACTION_UP -> touchUp()
        }
        return true
    }

    private fun touchStart(x: Float, y: Float) {
        mPath.moveTo(x, y)
        mX = x
        mY = y
    }

    private fun touchMove(x: Float, y: Float) {
        val dx = abs(x - mX)
        val dy = abs(y - mY)
        if (dx >= TOUCH_TOLERANCE || dy >= TOUCH_TOLERANCE) {
            // QuadTo() adds a quadratic bezier from the last point,
            // approaching control point (x1,y1), and ending at (x2,y2).
            mPath.quadTo(mX, mY, x, y)
            // Reset mX and mY to the last drawn point.
            mX = x
            mY = y
            mExtraCanvas.drawPath(mPath, mPaint)
        }
        invalidate()
    }

    private fun touchUp() {
        mPath.reset()
    }

    fun cleanCanvas() {
        setCanvasToDefaultState()
        invalidate()
    }

    fun saveBitmap(): Boolean {

        val rootDirPath = "${getExternalStoragePublicDirectory(DIRECTORY_PICTURES)}"

        val myDir = File("$rootDirPath${File.separator}Drawings")
        myDir.mkdirs()

        val imageName = "drawing${System.currentTimeMillis()}.jpg"
        val file = File(myDir, imageName)

        var out: FileOutputStream? = null
        return try {
            out = FileOutputStream(file)
            mExtraBitmap.compress(Bitmap.CompressFormat.JPEG, 90, out)
            context.sendBroadcast(Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, Uri.fromFile(file)))
            true
        } catch (e: Exception) {
            e.printStackTrace()
            false
        } finally {
            out?.flush()
            out?.close()
        }
    }
}