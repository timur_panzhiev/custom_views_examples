package com.itomych.customveiwsexamples.customviews

import android.content.Context
import android.content.res.TypedArray
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.util.AttributeSet
import android.view.View
import kotlin.math.cos
import kotlin.math.min
import kotlin.math.sin



private const val MARKER_RADIUS = 20f

class TumblerView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : View(context, attrs, defStyleAttr) {

    private var mWidth = 0f
    private var mSelectionCount = 6
    private var mHeight = 0f
    private var mTextPaint: Paint = Paint(Paint.ANTI_ALIAS_FLAG)
    private var mDialPaint: Paint = Paint(Paint.ANTI_ALIAS_FLAG)
    private var mRadius = 0f
    private var mActiveSelection = 0
    private var mOffColor = Color.GRAY
    private var mOnColor = Color.GREEN

    // String buffer for dial labels and float for ComputeXY result.
    private var mTempLabel = StringBuffer(8)
    private var mTempResult = FloatArray(2)

    init {
        readAttrs(attrs)
        setupTextPaint()
        setupDialPaint()
        setClickListener()
    }

    private fun readAttrs(attrs: AttributeSet?) {
        val typedArray: TypedArray = context.obtainStyledAttributes(
            attrs,
            com.itomych.customveiwsexamples.R.styleable.TumblerView,
            0, 0
        )
        applyAttrs(typedArray)
    }

    private fun applyAttrs(typedArray: TypedArray) {
        mOnColor = typedArray.getColor(
            com.itomych.customveiwsexamples.R.styleable.TumblerView_tv_on_color,
            mOnColor
        )
        mOffColor = typedArray.getColor(
            com.itomych.customveiwsexamples.R.styleable.TumblerView_tv_off_color,
            mOffColor
        )
        typedArray.recycle()
    }

    private fun setupTextPaint() {
        mTextPaint.apply {
            color = Color.BLACK
            style = Paint.Style.FILL_AND_STROKE
            textAlign = Paint.Align.CENTER

            textSize = 40f
        }
    }

    private fun setupDialPaint() {
        mDialPaint.color = mOffColor
    }

    private fun setClickListener() {
        setOnClickListener {
            mActiveSelection = (++mActiveSelection) % mSelectionCount

            if (mActiveSelection >= 1) {
                mDialPaint.color = mOnColor
            } else {
                mDialPaint.color = mOffColor
            }

            invalidate()
        }
    }

    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        mWidth = w.toFloat()
        mHeight = h.toFloat()
        mRadius = ((min(mWidth, mHeight) / 2 * 0.8).toFloat())
    }

    private fun computeXYForPosition(pos: Int, radius: Float, isLabel: Boolean = false): FloatArray {
        val result = mTempResult
        val startAngle: Double?
        val angle: Double
        if (mSelectionCount > 4) {
            startAngle = Math.PI * 1.5
            angle = startAngle + pos * (Math.PI / mSelectionCount)
            result[0] = (radius * cos(angle * 2)).toFloat() + mWidth / 2
            result[1] = (radius * sin(angle * 2)).toFloat() + mHeight / 2
            if (angle > Math.toRadians(360.0) && isLabel) {
                result[1] += 20f
            }
        } else {
            startAngle = Math.PI * (9 / 8.0)
            angle = startAngle + pos * (Math.PI / mSelectionCount)
            result[0] = (radius * cos(angle)).toFloat() + mWidth / 2
            result[1] = (radius * sin(angle)).toFloat() + mHeight / 2
        }
        return result
    }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)
        canvas.drawCircle(mWidth / 2, mHeight / 2, mRadius, mDialPaint)

        val labelRadius = mRadius + 20
        var label: String?
        for (i in 0 until mSelectionCount) {
            val xyData = computeXYForPosition(i, labelRadius, true)
            val x = xyData[0]
            val y = xyData[1]
            label = i.toString()
            canvas.drawText(label, 0, label.length, x, y, mTextPaint)
        }

        val distanceToMarker = mRadius - MARKER_RADIUS - 15f
        val xyData = computeXYForPosition(
            mActiveSelection,
            distanceToMarker
        )
        val x = xyData[0]
        val y = xyData[1]
        canvas.drawCircle(x, y, MARKER_RADIUS, mTextPaint)
    }

    fun setSelectionCount(count: Int) {
        this.mSelectionCount = count
        this.mActiveSelection = 0
        mDialPaint.color = mOffColor
        invalidate()
    }
}