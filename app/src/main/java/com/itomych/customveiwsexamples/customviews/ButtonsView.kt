package com.itomych.customveiwsexamples.customviews

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.view.animation.DecelerateInterpolator
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.constraintlayout.widget.ConstraintSet
import androidx.core.view.doOnLayout
import androidx.core.view.marginStart
import com.itomych.customveiwsexamples.R
import kotlinx.android.synthetic.main.layout_buttons_view.view.button_first as buttonFirst
import kotlinx.android.synthetic.main.layout_buttons_view.view.button_second as buttonSecond
import kotlinx.android.synthetic.main.layout_buttons_view.view.button_third as buttonThird
import kotlinx.android.synthetic.main.layout_buttons_view.view.slide_image as backgroundSlideImage


class ButtonsView @JvmOverloads constructor(
        context: Context,
        attrs: AttributeSet? = null,
        defStyleAttr: Int = 0
) : ConstraintLayout(context, attrs, defStyleAttr) {

    init {
        inflate(context, R.layout.layout_buttons_view, this)
        setupViews()
        setupListeners()
    }

    private fun setupViews() {
        doOnLayout {
            setupBackgroundView()
        }
    }

    private fun setupBackgroundView() {
        val set = ConstraintSet()
        set.clone(this)
        set.constrainWidth(R.id.slide_image, buttonFirst.width + convertDpToPx(16f))
        set.applyTo(this)
    }

    private fun setupListeners() {
        buttonFirst.setOnClickListener {
            setButtonsState(ButtonType.FIRST)
        }
        buttonSecond.setOnClickListener {
            setButtonsState(ButtonType.SECOND)
        }
        buttonThird.setOnClickListener {
            setButtonsState(ButtonType.THIRD)
        }
    }

    private fun setButtonsState(buttonType: ButtonType) {
        when (buttonType) {
            ButtonType.FIRST -> {
                setEnabledStateForButtons(false, true, true)
                translateBackgroundImage(getBackGroundImageXPosition(buttonFirst))
            }
            ButtonType.SECOND -> {
                setEnabledStateForButtons(true, false, true)
                translateBackgroundImage(getBackGroundImageXPosition(buttonSecond))
            }
            ButtonType.THIRD -> {
                setEnabledStateForButtons(true, true, false)
                translateBackgroundImage(getBackGroundImageXPosition(buttonThird))
            }
        }
    }

    private fun setEnabledStateForButtons(
            isFirstButtonEnabled: Boolean,
            isSecondButtonEnabled: Boolean,
            isThirdButtonEnabled: Boolean) {
        buttonFirst.isEnabled = isFirstButtonEnabled
        buttonSecond.isEnabled = isSecondButtonEnabled
        buttonThird.isEnabled = isThirdButtonEnabled
    }

    private fun translateBackgroundImage(valueFloat: Float) =
        backgroundSlideImage
                .animate()
                .setInterpolator(DecelerateInterpolator())
                .setDuration(300)
                .translationX(valueFloat)

    private fun getBackGroundImageXPosition(view: View) = (view.x - view.marginStart)

    private enum class ButtonType {
        FIRST, SECOND, THIRD
    }

    private fun convertDpToPx(dp: Float): Int {
        return (dp * context.resources.displayMetrics.density).toInt()
    }
}