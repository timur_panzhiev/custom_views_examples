package com.itomych.customveiwsexamples.customviews

import android.content.Context
import android.content.res.TypedArray
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.util.AttributeSet
import android.view.View
import android.view.animation.AccelerateDecelerateInterpolator
import com.itomych.customveiwsexamples.R


class ProgressCircle @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : View(context, attrs, defStyleAttr) {

//    private val gradientColors = intArrayOf(Color.GREEN, Color.YELLOW, Color.RED)
//    private val gradientPositions = floatArrayOf(0f, 0.5f, 1f)

    private lateinit var backCirclePaint: Paint
    private lateinit var foregroundCirclePaint: Paint
    private var strokeWidth: Float = 0f
    private var shouldAnimate: Boolean = true
    private var currentAngle: Float = 0f
    private val endAngle: Float = 250f
    private var animationStartTime = 0L
    private var interpolator = AccelerateDecelerateInterpolator()

    init {
        readAttributes(attrs)
        setupBackCirclePaint()
        setupForegroundCirclePaint()
    }

    private fun readAttributes(attrs: AttributeSet?) {
        val typedArray = context.obtainStyledAttributes(
            attrs,
            R.styleable.ProgressCircle,
            0, 0
        )
        applyAttributes(typedArray)
    }

    private fun applyAttributes(typedArray: TypedArray) {
        strokeWidth = typedArray.getDimension(
            R.styleable.ProgressCircle_pcv_strokeWidth,
            getDefaultStrokeWidth(context)
        )
        typedArray.recycle()
    }

    private fun setupBackCirclePaint() {
        backCirclePaint = Paint()
        backCirclePaint.isAntiAlias = true
        backCirclePaint.color = Color.LTGRAY
        backCirclePaint.style = Paint.Style.STROKE
        backCirclePaint.strokeWidth = strokeWidth
    }

    private fun setupForegroundCirclePaint() {
        foregroundCirclePaint = Paint()
        foregroundCirclePaint.isAntiAlias = true
        foregroundCirclePaint.color = Color.RED
        foregroundCirclePaint.strokeCap = Paint.Cap.ROUND
        foregroundCirclePaint.style = Paint.Style.STROKE
        foregroundCirclePaint.strokeWidth = strokeWidth
    }

    fun startAnimation() {
        shouldAnimate = true
        currentAngle = 0f
        animationStartTime = 0
        invalidate()
    }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)
        if (animationStartTime == 0L) {
            animationStartTime = System.currentTimeMillis()
        }
        canvas.drawCircle(
            width / 2f,
            height / 2f,
            width / 2f - strokeWidth / 2f,
            backCirclePaint
        )

        canvas.drawArc(
            0 + strokeWidth / 2,
            0 + strokeWidth / 2,
            width - strokeWidth / 2,
            height - strokeWidth / 2,
            -90f,
            if (shouldAnimate) getCurrentAngle() else endAngle,
            false,
            foregroundCirclePaint
        )

        if (shouldAnimate && currentAngle < endAngle) {
            invalidate()
        }
    }

    private fun getCurrentAngle(): Float {
        val now = System.currentTimeMillis()
        val pathGone = ((now - animationStartTime).toFloat() / DEFAULT_ANIMATION_TIME)
        val interpolatedPathGone = interpolator.getInterpolation(pathGone)
        currentAngle =
            if (pathGone < 1f) endAngle * interpolatedPathGone
            else endAngle
        return currentAngle
    }

    private fun getDefaultStrokeWidth(context: Context): Float =
        context.resources.displayMetrics.density * 10f

    companion object {
        @JvmField
        val TAG = ProgressCircle::class.java.name
        const val DEFAULT_ANIMATION_TIME = 1000
    }
}