package com.itomych.customveiwsexamples.customviews.drawing

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.Path
import android.graphics.Rect
import android.graphics.RectF
import android.graphics.Region
import android.os.Build
import android.util.AttributeSet
import android.view.View
import com.itomych.customveiwsexamples.R

class ClippedView @JvmOverloads constructor(
    context: Context,
    attributeSet: AttributeSet? = null,
    defStyleAttr: Int = 0
) : View(context, attributeSet, defStyleAttr) {

    private var mPaint: Paint = Paint()
    private var mPath: Path = Path()
    private var mRectF: RectF? = null

    private val mClipRectRight =
        resources.getDimension(R.dimen.clipRectRight)
    private val mClipRectBottom =
        resources.getDimension(R.dimen.clipRectBottom)
    private val mClipRectTop =
        resources.getDimension(R.dimen.clipRectTop)
    private val mClipRectLeft =
        resources.getDimension(R.dimen.clipRectLeft)
    private val mRectInset =
        resources.getDimension(R.dimen.rectInset)
    private val mSmallRectOffset =
        resources.getDimension(R.dimen.smallRectOffset)

    private val mCircleRadius =
        resources.getDimension(R.dimen.circleRadius)

    private val mTextOffset =
        resources.getDimension(R.dimen.textOffset)
    private val mTextSize = resources.getDimension(R.dimen.textSize)

    private val mColumnOne = mRectInset
    private val mColumnTwo = mColumnOne + mRectInset + mClipRectRight

    private val mRowOne = mRectInset
    private val mRowTwo = mRowOne + mRectInset + mClipRectBottom
    private val mRowThree = mRowTwo + mRectInset + mClipRectBottom
    private val mRowFour = mRowThree + mRectInset + mClipRectBottom
    private val mTextRow = mRowFour + (1.5f * mClipRectBottom)

    init {
        setupPaint()
    }

    private fun setupPaint() {

        mPaint.apply {
            isAntiAlias = true
            strokeWidth =
                resources.getDimension(com.itomych.customveiwsexamples.R.dimen.strokeWidth)
            textSize = resources.getDimension(R.dimen.textSize)
        }

        mRectF = RectF(
            Rect(
                mRectInset.toInt(),
                mRectInset.toInt(),
                (mClipRectRight - mRectInset).toInt(),
                (mClipRectBottom - mRectInset).toInt()
            )
        )
    }

    private fun drawClippedRectangle(canvas: Canvas) {
        canvas.clipRect(
            mClipRectLeft,
            mClipRectTop,
            mClipRectRight,
            mClipRectBottom
        )
        // Fill the canvas with white.
        // With the clipped rectangle, this only draws
        // inside the clipping rectangle.
        // The rest of the surface remains gray.
        canvas.drawColor(Color.WHITE)

        // Change the color to red and
        // draw a line inside the clipping rectangle.
        mPaint.color = Color.RED
        canvas.drawLine(
            mClipRectLeft,
            mClipRectTop,
            mClipRectRight,
            mClipRectBottom,
            mPaint
        )

        // Set the color to green and
        // draw a circle inside the clipping rectangle.
        mPaint.color = Color.GREEN
        canvas.drawCircle(
            mCircleRadius,
            mClipRectBottom - mCircleRadius,
            mCircleRadius,
            mPaint
        )

        // Set the color to blue and draw text aligned with the right edge
        // of the clipping rectangle.
        mPaint.color = Color.BLUE
        // Align the RIGHT side of the text with the origin.
        mPaint.textAlign = Paint.Align.RIGHT
        canvas.drawText(
            context.getString(com.itomych.customveiwsexamples.R.string.clipping),
            mClipRectRight,
            mTextOffset,
            mPaint
        )
    }

    override fun onDraw(canvas: Canvas) {

        canvas.drawColor(Color.GRAY)
        canvas.save()
        canvas.translate(mColumnOne, mRowOne)
        drawClippedRectangle(canvas)
        canvas.restore()

        canvas.save()
        canvas.translate(mColumnTwo, mRowOne)
        // Use the subtraction of two clipping rectangles to create a frame.
        canvas.clipRect(
            2 * mRectInset,
            2 * mRectInset,
            mClipRectRight - 2 * mRectInset,
            mClipRectBottom - 2 * mRectInset
        )
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.O)
            canvas.clipRect(
                4 * mRectInset,
                4 * mRectInset,
                mClipRectRight - 4 * mRectInset,
                mClipRectBottom - 4 * mRectInset,
                Region.Op.DIFFERENCE
            )
        else {
            canvas.clipOutRect(
                4 * mRectInset, 4 * mRectInset,
                mClipRectRight - 4 * mRectInset,
                mClipRectBottom - 4 * mRectInset
            )
        }

        drawClippedRectangle(canvas)
        canvas.restore()

        canvas.save()
        canvas.translate(mColumnOne, mRowTwo)
        // Clears any lines and curves from the path but unlike reset(),
        // keeps the internal data structure for faster reuse.
        mPath.rewind()
        mPath.addCircle(
            mCircleRadius,
            mClipRectBottom - mCircleRadius,
            mCircleRadius,
            Path.Direction.CCW
        )

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.O) {
            canvas.clipPath(mPath, Region.Op.DIFFERENCE)
        } else {
            canvas.clipOutPath(mPath)
        }

        drawClippedRectangle(canvas)
        canvas.restore()

        // Use the intersection of two rectangles as the clipping region.
        canvas.save()
        canvas.translate(mColumnTwo, mRowTwo)
        canvas.clipRect(
            mClipRectLeft,
            mClipRectTop,
            mClipRectRight - mSmallRectOffset,
            mClipRectBottom - mSmallRectOffset
        )
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.O) {
            canvas.clipRect(
                mClipRectLeft + mSmallRectOffset,
                mClipRectTop + mSmallRectOffset,
                mClipRectRight,
                mClipRectBottom,
                Region.Op.INTERSECT
            )
        } else {
            canvas.clipRect(
                mClipRectLeft + mSmallRectOffset,
                mClipRectTop + mSmallRectOffset,
                mClipRectRight,
                mClipRectBottom
            )
        }

        drawClippedRectangle(canvas)
        canvas.restore()

        // You can combine shapes and draw any path to define a clipping region.
        canvas.save()
        canvas.translate(mColumnOne, mRowThree)
        mPath.rewind()
        mPath.addCircle(
            mClipRectLeft + mRectInset + mCircleRadius,
            mClipRectTop + mCircleRadius + mRectInset,
            mCircleRadius,
            Path.Direction.CCW
        )
        mPath.addRect(
            mClipRectRight / 2 - mCircleRadius,
            mClipRectTop + mCircleRadius + mRectInset,
            mClipRectRight / 2 + mCircleRadius,
            mClipRectBottom - mRectInset,
            Path.Direction.CCW
        )
        canvas.clipPath(mPath)
        drawClippedRectangle(canvas)
        canvas.restore()

        // Use a rounded rectangle. Use mClipRectRight/4 to draw a circle.
        canvas.save()
        canvas.translate(mColumnTwo, mRowThree)
        mPath.rewind()
        mPath.addRoundRect(
            mRectF,
            mClipRectRight / 4,
            mClipRectRight / 4,
            Path.Direction.CCW
        )
        canvas.clipPath(mPath)
        drawClippedRectangle(canvas)
        canvas.restore()

        // Clip the outside around the rectangle.
        canvas.save()
        // Move the origin to the right for the next rectangle.
        canvas.translate(mColumnOne, mRowFour)
        canvas.clipRect(
            2 * mRectInset,
            2 * mRectInset,
            mClipRectRight - 2 * mRectInset,
            mClipRectBottom - 2 * mRectInset
        )
        drawClippedRectangle(canvas)
        canvas.restore()

        // Draw text with a translate transformation applied.
        canvas.save()
        mPaint.color = Color.CYAN
        // Align the RIGHT side of the text with the origin.
        mPaint.textAlign = Paint.Align.LEFT
        // Apply transformation to canvas.
        canvas.translate(mColumnTwo, mTextRow)
        // Draw text.
        canvas.drawText(
            context.getString(R.string.translated), 0f, 0f, mPaint
        )
        canvas.restore()

        // Draw text with a translate and skew transformations applied.
        canvas.save()
        mPaint.textSize = mTextSize
        mPaint.textAlign = Paint.Align.RIGHT
        // Position text.
        canvas.translate(mColumnTwo, mTextRow)
        // Apply skew transformation.
        canvas.skew(0.2f, 0.3f)
        canvas.drawText(
            context.getString(R.string.skewed), 0f, 0f, mPaint
        )
        canvas.restore()
    } // End of onDraw()
}