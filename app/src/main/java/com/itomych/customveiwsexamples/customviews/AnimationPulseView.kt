package com.itomych.customveiwsexamples.customviews

import android.animation.AnimatorSet
import android.animation.ObjectAnimator
import android.animation.ValueAnimator
import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.View
import android.view.animation.LinearInterpolator

class PulseAnimationView @JvmOverloads constructor(context: Context, attrs: AttributeSet? = null) :
    View(context, attrs) {

    private var mX = 0f
    private var mY = 0f
    private var mRadius = 0f

    private val mPaint = Paint()
    private val mPulseAnimatorSet = AnimatorSet()

    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        // This method is called when the size of the view changes.
        // For this app, it is only called when the activity is started or restarted.
        // getWidth() cannot return anything valid in onCreate(), but it does here.
        // We create the animators and animator set here once, and handle the starting and
        // canceling in the event handler.

        // Animate the "radius" property with an ObjectAnimator,
        // giving it an interpolator and duration.
        // This animator creates an increasingly larger circle from a
        // radius of 0 to the width of the view.
        val growAnimator = ObjectAnimator.ofFloat(
            this,
            "radius",
            0f, width.toFloat() / 2
        ).apply {
            duration = ANIMATION_DURATION
            interpolator = LinearInterpolator()
            repeatMode = ValueAnimator.REVERSE
            repeatCount = 1
        }

        val alphaAnimator = ObjectAnimator.ofFloat(
            this,
            "alpha",
            1f, 0f
        ).apply {
            duration = ANIMATION_DURATION
            interpolator = LinearInterpolator()
            repeatMode = ValueAnimator.REVERSE
            repeatCount = 1
        }

        // Create a second animator to
        // animate the "radius" property with an ObjectAnimator,
        // giving it an interpolator and duration.
        // This animator creates a shrinking circle
        // from a radius of the view's width to 0.
        // Add a delay to starting the animation.
//        val shrinkAnimator = ObjectAnimator.ofFloat(
//            this,
//            "radius",
//            width.toFloat(), 0f
//        ).apply {
//            duration = ANIMATION_DURATION
//            interpolator = LinearOutSlowInInterpolator()
//            startDelay = ANIMATION_DELAY
//        }

        // If you don't need a delay between the two animations,
        // you can use one animator that repeats in reverse.
        // Uses the default AccelerateDecelerateInterpolator.
//        val repeatAnimator = ObjectAnimator.ofFloat(
//            this,
//            "radius",
//            0f, width.toFloat()
//        ).apply {
//            startDelay = ANIMATION_DELAY
//            duration = ANIMATION_DURATION.toLong()
//            repeatCount = 1
//            repeatMode = ValueAnimator.REVERSE
//        }

        // Create an AnimatorSet to combine the two animations into a sequence.
        // Play the expanding circle, wait, then play the shrinking circle.
        mPulseAnimatorSet.play(growAnimator).with(alphaAnimator)
//        mPulseAnimatorSet.play(repeatAnimator).after(shrinkAnimator)
    }

    override fun onTouchEvent(event: MotionEvent): Boolean {
        if (event.actionMasked == MotionEvent.ACTION_DOWN) {

            // Where the center of the circle will be.
            mX = event.x
            mY = event.y

            // If there is an animation running, cancel it.
            // This resets the AnimatorSet and its animations to the starting values.
            if (mPulseAnimatorSet.isRunning) {
                mPulseAnimatorSet.cancel()
            }
            // Start the animation sequence.
            mPulseAnimatorSet.start()
        }
        return super.onTouchEvent(event)
    }

    /**
     * Required setter for the animated property.
     * Called by the Animator to update the property.
     *
     * @param radius This view's radius property.
     */
    fun setRadius(radius: Float) {
        mRadius = radius
        // Calculate a new color from the radius.
        mPaint.color = Color.GREEN + radius.toInt() / COLOR_ADJUSTER
        // Updating the property does not automatically redraw.
        invalidate()
    }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)
        canvas.drawCircle(mX, mY, mRadius, mPaint)
    }

    companion object {

        private const val ANIMATION_DURATION = 2000L
        private const val ANIMATION_DELAY: Long = 1000
        private const val COLOR_ADJUSTER = 5
    }
}