package com.itomych.customveiwsexamples.surfaceView.customview

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.Path
import android.graphics.RectF
import android.graphics.Region
import android.os.Build
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.SurfaceView
import com.itomych.customveiwsexamples.surfaceView.model.FlashlightCone
import kotlin.math.floor

class GameView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : SurfaceView(context, attrs, defStyleAttr), Runnable {

    private lateinit var mFlashlightCone: FlashlightCone
    private var mRunning: Boolean = false
    private var mGameThread: Thread? = null
    private val mPaint = Paint()
    private val mPath = Path()
    private lateinit var mWinnerRect: RectF
    private val mSurfaceHolder = holder
    private lateinit var mBitmap: Bitmap
    private var mBitmapX = 0f
    private var mBitmapY = 0f
    private var mViewWidth = 0
    private var mViewHeight = 0

    init {
        mPaint.color = Color.DKGRAY
    }

    override fun run() {
        var canvas: Canvas?
        while (mRunning) {
            if (mSurfaceHolder.surface.isValid) {
                val x = mFlashlightCone.mX
                val y = mFlashlightCone.mY
                val radius = mFlashlightCone.mRadius
                try {
                    canvas = mSurfaceHolder.lockCanvas()
                    canvas.save()
                    canvas.drawColor(Color.WHITE)
                    canvas.drawBitmap(mBitmap, mBitmapX, mBitmapY, mPaint)
                    mPath.addCircle(x, y, radius, Path.Direction.CCW)
                    if (Build.VERSION.SDK_INT < Build.VERSION_CODES.O) {
                        canvas.clipPath(mPath, Region.Op.DIFFERENCE)
                    } else {
                        canvas.clipOutPath(mPath)
                    }

                    canvas.drawColor(Color.BLACK)

                    if (x > mWinnerRect.left && x < mWinnerRect.right
                        && y > mWinnerRect.top && y < mWinnerRect.bottom
                    ) {
                        canvas.drawColor(Color.WHITE)
                        canvas.drawBitmap(mBitmap, mBitmapX, mBitmapY, mPaint)
                        canvas.drawText(
                            "WIN!", mViewWidth / 3f, mViewHeight / 2f, mPaint
                        )
                    }

                    mPath.rewind()
                    canvas.restore()
                    mSurfaceHolder.unlockCanvasAndPost(canvas)

                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        }
    }

    fun onPause() {
        mRunning = false
        try {
            // Stop the thread (rejoin the main thread)
            mGameThread?.join()
        } catch (e: InterruptedException) {

        }
    }

    fun onResume() {
        mRunning = true
        mGameThread = Thread(this)
        mGameThread?.start()
    }

    private fun setUpBitmap() {
        mBitmapX = floor(Math.random() * (mViewWidth - mBitmap.width)).toFloat()
        mBitmapY = floor(Math.random() * (mViewHeight - mBitmap.height)).toFloat()
        mWinnerRect = RectF(
            mBitmapX,
            mBitmapY,
            mBitmapX + mBitmap.width,
            mBitmapY + mBitmap.height
        )
    }

    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        super.onSizeChanged(w, h, oldw, oldh)
        mViewWidth = w
        mViewHeight = h
        mFlashlightCone = FlashlightCone(mViewWidth.toFloat(), mViewHeight.toFloat())
        mPaint.textSize = mViewHeight / 5f
        mBitmap = BitmapFactory.decodeResource(
            context.resources, com.itomych.customveiwsexamples.R.drawable.andr_scate
        )
        setUpBitmap()
    }

    override fun onTouchEvent(event: MotionEvent): Boolean {
        val x = event.x
        val y = event.y

        // Invalidate() is inside the case statements because there are
        // many other motion events, and we don't want to invalidate
        // the view for those.
        when (event.action) {
            MotionEvent.ACTION_DOWN -> {
                setUpBitmap()
                updateFrame(x, y)
//                invalidate()
            }
            MotionEvent.ACTION_MOVE -> {
                updateFrame(x, y)
//                invalidate()
            }
        }
        return true
    }

    private fun updateFrame(newX: Float, newY: Float) {
        mFlashlightCone.update(newX, newY)
    }
}