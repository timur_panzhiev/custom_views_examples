package com.itomych.customveiwsexamples.surfaceView

import android.content.pm.ActivityInfo
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.itomych.customveiwsexamples.surfaceView.customview.GameView

class SurfaceViewActivity : AppCompatActivity() {

    private lateinit var mGameView: GameView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE
        mGameView = GameView(this)
        setContentView(mGameView)
    }

    override fun onPause() {
        super.onPause()
        mGameView.onPause()

    }

    override fun onResume() {
        super.onResume()
        mGameView.onResume()
    }
}
