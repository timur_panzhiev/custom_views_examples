package com.itomych.customveiwsexamples.surfaceView.model

import kotlin.math.min

class FlashlightCone (viewWidth: Float, viewHeight: Float) {

    var mX = 0f
        private set
    var mY = 0f
        private set
    var mRadius = 0f
        private set

    init {
        mX = viewWidth / 2
        mY = viewHeight / 2
        mRadius = min(viewWidth, viewHeight) / 3
    }

    fun update(newX: Float, newY: Float) {
        mX = newX
        mY = newY
    }
}