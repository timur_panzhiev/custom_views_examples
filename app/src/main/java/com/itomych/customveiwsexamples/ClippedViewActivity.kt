package com.itomych.customveiwsexamples

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.itomych.customveiwsexamples.customviews.drawing.ClippedView

class ClippedViewActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(ClippedView(this))
    }
}
