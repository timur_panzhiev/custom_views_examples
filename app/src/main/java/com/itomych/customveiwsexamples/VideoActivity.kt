package com.itomych.customveiwsexamples

import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.view.View
import android.webkit.URLUtil
import android.widget.MediaController
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_video.buffering_textview
import kotlinx.android.synthetic.main.activity_video.videoview

private const val VIDEO_SAMPLE = "https://developers.google.com/training/images/tacoma_narrows.mp4"
private const val PLAYBACK_TIME = "play_time"

class VideoActivity : AppCompatActivity() {

    private var mCurrentPosition = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_video)
        if (savedInstanceState != null) {
            mCurrentPosition = savedInstanceState.getInt(PLAYBACK_TIME)
        }
        val controller = MediaController(this)
        controller.setMediaPlayer(videoview)
        videoview.setMediaController(controller)

    }

    private fun initializePlayer() {
        buffering_textview.visibility = View.VISIBLE

        val videoUri = getMedia(VIDEO_SAMPLE)
        videoview.apply {
            setVideoURI(videoUri)

            setOnPreparedListener {
                buffering_textview.visibility = View.INVISIBLE

                if (mCurrentPosition > 0) {
                    seekTo(mCurrentPosition)
                } else {
                    seekTo(1)
                }

                start()
            }
            setOnCompletionListener {
                Toast.makeText(context, "Playback completed",
                        Toast.LENGTH_SHORT).show()
                seekTo(1)
            }
        }
    }

    private fun getMedia(mediaName: String): Uri =
            if (URLUtil.isValidUrl(mediaName)) {
                // media name is an external URL
                Uri.parse(mediaName)
            } else { // media name is a raw resource embedded in the app
                Uri.parse("android.resource://" + packageName +
                        "/raw/" + mediaName)
            }

    private fun releasePlayer() {
        videoview.stopPlayback()
    }

    override fun onStart() {
        super.onStart()
        initializePlayer()
    }

    override fun onPause() {
        super.onPause()
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.N) {
            videoview.pause()
        }
    }

    override fun onStop() {
        super.onStop()
        releasePlayer()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)

        outState.putInt(PLAYBACK_TIME, videoview.currentPosition)
    }
}
